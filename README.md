# Dockerun Electron

**The Dockerun you love, built for offline usage on Windows, macOS and Linux**

![](assets/screen.png)

## Use it

Head [here](https://gitlab.com/fsorge/dockerun-electron/-/releases) and choose the latest version.



## Contribute

First of all, clone it using SSH

```bash
git clone git@gitlab.com:fsorge/dockerun-electron.git
```

Then, install project dependencies:

```bash
npm install
npm run build
```

To start the development application:

```bash
npm run start
```

**That's all! Start doing your own experiments and improvements!**

**Note:** We suggest you to work in your own branch to avoid conflicts.



## Submit changes for review and approval

Before doing that, check that the application compiles correctly:

```bash
npm run make
```

If the application compiles correctly, it's time to submit a merge request. Use GitLab to do that.

The owner of the repository will then check your changes and if everything is right, your branch will be merged with the official website.

It would be better if you could check on Windows, macOS and Linux if your changes work perfectly.
